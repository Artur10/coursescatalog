﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CoursesCatalog.DAL.Repositories
{
    public class RepositoryGeneric<T> : IRepositoryGeneric<T> where T : class
    {
        private readonly IDataContext _dataContext;
  
        public RepositoryGeneric(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        

        public T Add(T entity)
        {
            return
                entity == null ?
                    throw new ArgumentNullException(nameof(entity)) :
                    _dataContext.Set<T>().Add(entity).Entity;
        }

        public T Delete(T entity)
        {
            return
                entity == null
                    ? throw new ArgumentNullException(nameof(entity))
                    : _dataContext.Set<T>().Remove(entity).Entity;
        }

        public T Delete(int id)
        {
            return _dataContext.Set<T>().Remove(GetById(id)).Entity;
        }

        public IEnumerable<T> GetAll()
        {
            return _dataContext.Set<T>().ToList();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var asyncTocken = _dataContext.Set<T>().ToListAsync();
            return await asyncTocken;
        }

        public IEnumerable<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            return
                predicate == null ?
                    throw new ArgumentNullException(nameof(predicate)) :
                    _dataContext.Set<T>().Where(predicate);
        }

        public async Task<IEnumerable<T>> GetByAsync(Expression<Func<T, bool>> predicate)
        {
            return
                predicate == null ?
                throw new ArgumentNullException(nameof(predicate)) :
                 await _dataContext.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetByAsyncWithNoTracking(Expression<Func<T, bool>> predicate)
        {
            return
                predicate == null ?
                throw new ArgumentNullException(nameof(predicate)) :
                 await _dataContext.Set<T>().AsNoTracking().Where(predicate).ToListAsync();
        }

        public T GetById(int id)
        {
            return _dataContext.Set<T>().Find(id);
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var asyncTocken = _dataContext.Set<T>().FindAsync(id);
            return await asyncTocken;
        }

        public int Save()
        {
            return _dataContext.SaveChanges();
        }

        public Task<int> SaveAsync()
        {
            var asyncTocken = _dataContext.SaveChangesAsync();
            return asyncTocken;
        }

        public void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            else
                _dataContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
