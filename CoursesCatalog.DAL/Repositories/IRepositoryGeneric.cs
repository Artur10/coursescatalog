﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CoursesCatalog.DAL.Repositories
{
    public interface IRepositoryGeneric<T> where T:class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetBy(Expression<Func<T, bool>> predicate);
        T GetById(int id);
        T Add(T entity);
        void Update(T entity);
        T Delete(T entity);
        T Delete(int id);
        int Save();

        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetByAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetByAsyncWithNoTracking(Expression<Func<T, bool>> predicate);
        Task<T> GetByIdAsync(int id);
        Task<int> SaveAsync();
    }
}
