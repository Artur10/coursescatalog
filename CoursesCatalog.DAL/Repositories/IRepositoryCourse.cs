﻿using CoursesCatalog.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoursesCatalog.DAL.Repositories
{
    public interface IRepositoryCourse:IRepositoryGeneric<Course>
    {
    }
}
