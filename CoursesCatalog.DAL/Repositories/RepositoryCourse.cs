﻿using CoursesCatalog.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoursesCatalog.DAL.Repositories
{
    public class RepositoryCourse:RepositoryGeneric<Course>,IRepositoryCourse
    {
        public RepositoryCourse(DataContext context) : base(context) { }
    }
}
