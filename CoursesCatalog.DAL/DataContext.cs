﻿using CoursesCatalog.Models;
using Microsoft.EntityFrameworkCore;

namespace CoursesCatalog.DAL
{
    public class DataContext:DbContext,IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Course> Courses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Course");
        }

    }
}
