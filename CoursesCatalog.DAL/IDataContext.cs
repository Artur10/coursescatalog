﻿using CoursesCatalog.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;

namespace CoursesCatalog.DAL
{
    public interface IDataContext
    {
       DbSet<Course> Courses { get; set; }
       DbSet<TEntity> Set<TEntity>() where TEntity : class;
       EntityEntry Entry(object entity);
       int SaveChanges();
       Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
