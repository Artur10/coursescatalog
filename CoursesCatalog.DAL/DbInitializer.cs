﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CoursesCatalog.Models;

namespace CoursesCatalog.DAL
{
    public class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            if (context.Courses.Any())
            {
                return;
            }

            var courses = new Course[]
            {
                new Course{Title="Chemistry", Description = "Course describe the basics of chemistry",DayOfWeek = 1,StartHour = new DateTime(2018,2,1,09,0,0),EndHour= new DateTime(2018,2,1,11,0,0),Price = 20 },
                new Course{Title="Microeconomics", Description = "Course describe the basics of microeconomics",DayOfWeek = 3,StartHour = new DateTime(2018,2,1,11,0,0),EndHour= new DateTime(2018,2,1,13,0,0),Price = 35},
            };
            foreach (Course c in courses)
            {
                context.Courses.Add(c);
            }
            context.SaveChanges();

        }
    }
}
