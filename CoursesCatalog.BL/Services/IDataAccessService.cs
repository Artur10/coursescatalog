﻿using CoursesCatalog.Models;
using CoursesCatalog.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoursesCatalog.BL.Services
{
    public interface IDataAccessService
    {
        Task<IEnumerable<CourseViewModel>> GetAllCoursesAsync();
        Task<bool> TryAddNewCoureAsync(CourseViewModel coure);
        Task<bool> TryUpdateCoureAsync(CourseViewModel course);
        Task<CourseViewModel> GetCourseById(int courseId);
        Task DeleteCoureAsync(int courseId);
    }
}
