﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoursesCatalog.BL.Utils;
using CoursesCatalog.DAL.Repositories;
using CoursesCatalog.Models;
using CoursesCatalog.Models.ViewModels;

namespace CoursesCatalog.BL.Services
{
    public class DataAccessService : IDataAccessService
    {
        private IRepositoryCourse _repositoryCourse;
        public DataAccessService(IRepositoryCourse repositoryCourse)
        {
            _repositoryCourse = repositoryCourse;
        }

        public async Task<IEnumerable<CourseViewModel>> GetAllCoursesAsync()
        {
            var asyncTocken = _repositoryCourse.GetAllAsync();
            var courses = await asyncTocken;
            var coursesViewModel =  courses.Select(c => c.ToViewModel());
            return coursesViewModel;
        }
        public async Task<bool> TryAddNewCoureAsync(CourseViewModel courseViewModel)
        {
            var course = courseViewModel.ToModel();
            var overlap = await IsCourseOverlap(course);
            if (!overlap)
            {
                _repositoryCourse.Add(course);
                var result = await _repositoryCourse.SaveAsync();
            }
            return overlap;
        }
        public async Task<bool> TryUpdateCoureAsync(CourseViewModel courseViewModel)
        {
            var course = courseViewModel.ToModel();
            var overlap = await IsCourseOverlap(course);
            if (!overlap)
            {
                _repositoryCourse.Update(course);
                await _repositoryCourse.SaveAsync();
            }
            return overlap;
        }
        public async Task<CourseViewModel> GetCourseById(int courseId)
        {
            var courseToEdit = await _repositoryCourse.GetByIdAsync(courseId);
            var courseViewModelToEdit = courseToEdit.ToViewModel();
            return courseViewModelToEdit;
        }

        public async Task DeleteCoureAsync(int courseId)
        {
            _repositoryCourse.Delete(courseId);
            await _repositoryCourse.SaveAsync();
        }

        private async Task<bool> IsCourseOverlap(Course course)
        {
            var dayCourses = await _repositoryCourse.GetByAsyncWithNoTracking(c => c.DayOfWeek == course.DayOfWeek && c.ID != course.ID);
            bool overlap = dayCourses.Any(c => c.StartHour < course.EndHour && course.StartHour <= c.EndHour);
            if (overlap)
            {
                return true;
            }
            return false;
        }
    }
}
