﻿using CoursesCatalog.Models;
using CoursesCatalog.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace CoursesCatalog.BL.Utils
{
    public static class CourseCatalogExtensions
    {
        private static Dictionary<string, int> dayNumberDictionary = new Dictionary<string, int>()
        {
                {"Monday",1},
                {"Tuesday",2},
                {"Wednesday",3},
                {"Thursday",4},
                {"Friday",5},
                {"Saturday",6},
                {"Sanday",7}
        };

        public static CourseViewModel ToViewModel(this Course course)
        {
            CourseViewModel courseViewModel = new CourseViewModel()
            {
                ID = course.ID,
                Title = course.Title,
                Description = course.Description,
                StartHour = course.StartHour,
                EndHour = course.EndHour,
                DayOfWeek = GetDayName(course.DayOfWeek),
                Price = course.Price
            };

            return courseViewModel;
        }
        public static Course ToModel(this CourseViewModel courseViewModel)
        {
            Course course = new Course()
            {
                ID = courseViewModel.ID,
                Title = courseViewModel.Title,
                Description = courseViewModel.Description,
                DayOfWeek = GetDayNumber(courseViewModel.DayOfWeek),
                StartHour = courseViewModel.StartHour,
                EndHour = courseViewModel.EndHour,
                Price = courseViewModel.Price
            };
            return course;
        }
        private static int GetDayNumber(string dayName)
        {
            int index;
            dayNumberDictionary.TryGetValue(dayName,out index);
            return index;
        }
        private static string GetDayName(int dayIndex)
        {
            string dayName = dayNumberDictionary.FirstOrDefault(i => i.Value == dayIndex).Key;
            return dayName; 
        }
    }
}
