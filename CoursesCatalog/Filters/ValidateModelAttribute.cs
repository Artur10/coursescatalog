﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CoursesCatalog.Filters
{
    public class ValidateModelAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string errorMessage = "Model is not valid.Please verify input parametrs";
            if (!context.ModelState.IsValid)
            {
                context.Result = new RedirectToActionResult("Error", "Home", errorMessage);
            }
        }

    }
}
