﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace CoursesCatalog.Filters
{
    public class GlobalExceptionFilterAttribute :Attribute, IExceptionFilter
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public GlobalExceptionFilterAttribute()
        {

        }
        public GlobalExceptionFilterAttribute(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public void OnException(ExceptionContext context)
        {
            if (!_hostingEnvironment.IsDevelopment())
            {
                return;
            }
            var result = new ViewResult { ViewName = "CustomErrorPage" };
        }
    }
}
