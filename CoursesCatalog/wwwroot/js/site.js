﻿$(document).ready(function () {
    initializeTimeTable();
});
$('[data-courseid]').click(function () {
    var selectedItemId = $(this).data("courseid")
    var fullUrl = buildUri(selectedItemId);
    $.get(fullUrl, function (data) {
        $('#updated-content-container').html(data);
        $('#editCourseModal').modal('show');
    }).fail(function () {
        alert("error");
    });
});
$('#add-course').submit(function (e) {
    var startHour = $('#StartHour').val();
    var endHour = $('#EndHour').val();
    if ((endHour <= startHour) || ((endHour - startHour) < 1)) {
        alert("Please validate your course time")
        e.preventDefault();
    }
});
function buildUri(itemid) {
    var baseUrl = window.location.protocol + "//" + window.location.host + "/Home/GetCourseToEdit"
    var fullUrl = baseUrl + "?courseId=" + itemid;
    return fullUrl;
}
function initializeTimeTable() {
    var items = $('.json_data');
    var jsonDataArray = [];

    $.each(items, function (index, item) {
        jsonDataArray.push($(item).data('jsonvalue'));
    });

    var timetable = new Timetable();
    timetable.setScope(9, 17); // optional, only whole hours between 0 and 23
    timetable.addLocations(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sanday']);

    jsonDataArray.forEach(function (item) {
        timetable.addEvent(item.eventName, item.day, new Date(2015, 7, 17, item.startHour), new Date(2015, 7, 17, item.endHour));
    });

    var renderer = new Timetable.Renderer(timetable);
    renderer.draw('.timetable'); // any css selector

    $('.timetable').hide().show(1000);
}
