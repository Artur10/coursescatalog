﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoursesCatalog.Models;
using CoursesCatalog.BL.Services;
using CoursesCatalog.Models.ViewModels;
using CoursesCatalog.Filters;

namespace CoursesCatalog.Controllers
{
    [GlobalExceptionFilter]
    public class HomeController : Controller
    {
        private IDataAccessService _dataAccessService;
        public HomeController(IDataAccessService dataAccessService)
        {
            _dataAccessService = dataAccessService;
        }

        public async Task<IActionResult> Index()
        {
            var asyncTocken = _dataAccessService.GetAllCoursesAsync();
            var coursesData = await asyncTocken;
            return View(coursesData);
        }

        [ValidateModel]
        public async Task<IActionResult> AddNewCourse(CourseViewModel courseViewModel)
        {
            var opperationNotSuccess = await _dataAccessService.TryAddNewCoureAsync(courseViewModel);
            if (!opperationNotSuccess)
            {
                var courses = await _dataAccessService.GetAllCoursesAsync();
                return View("~/Views/Home/Index.cshtml", courses);
            }
            return View("Error", new ErrorViewModel() { Message = "Course overlaps" });
        }

        public async Task<IActionResult> DeleteCourse(int courseId)
        {
            await _dataAccessService.DeleteCoureAsync(courseId);
            var asyncTocken = _dataAccessService.GetAllCoursesAsync();
            var coursesData = await asyncTocken;
            return View("~/Views/Home/Index.cshtml", coursesData);
        }

        public async Task<IActionResult> GetCourseToEdit(int courseId)
        {
            var courseViewModel = await _dataAccessService.GetCourseById(courseId);
            return PartialView("~/Views/Partial/_EditCourse.cshtml",courseViewModel);
        }

        [ValidateModel]
        public async Task<IActionResult> UpdateCourse(CourseViewModel courseViewModel)
        {
            var operationNotSuccess = await _dataAccessService.TryUpdateCoureAsync(courseViewModel);
            if (!operationNotSuccess)
            {
                var courses = await _dataAccessService.GetAllCoursesAsync();
                return View("~/Views/Home/Index.cshtml", courses);
            }
            return View("~/Views/Shared/Error.cshtml",new ErrorViewModel() {Message = "Course overlaps"});
        }

        public IActionResult Error(string errorMessage)
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier, Message = errorMessage});
        }
    }
}
