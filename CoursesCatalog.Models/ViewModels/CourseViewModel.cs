﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoursesCatalog.Models.ViewModels
{
    public sealed class CourseViewModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [StringLength(40)]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartHour { get; set; }

        [DataType(DataType.Date)]
        public DateTime EndHour { get; set; }

        [Required]
        public string DayOfWeek { get; set; }

        [Required]
        public int Price { get; set; }
    }
}
