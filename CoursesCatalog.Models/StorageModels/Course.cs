﻿using System;

namespace CoursesCatalog.Models
{
    public sealed class Course
   {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int DayOfWeek { get; set; } 
        public DateTime StartHour { get; set; }
        public DateTime EndHour { get; set; }
        public int Price { get; set; }
   }
}
