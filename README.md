# README #

### Course Catalog Application ###

Application main page that shows already added courses in timeline

![timeline](https://i.imgur.com/Dy74vgp.png)

List of saved courses

![courses](https://i.imgur.com/yYEhglm.png)


Add new course modal window

![add course](https://i.imgur.com/GVMYZeE.png)

Edit course modal wndow

![edit course](https://i.imgur.com/aUKDYRt.png)


### Technologies Stack ###
* ASP.NET CORE
* EF Core
* jQuery
* Bootstrap


### Author ###
* Developed by Artur Lavrov: arturstylus@gmail.com